import pandas as pd
import numpy as np

def BasicInfo(data):
    for column in data.columns:
        values = data[column].unique()

        if (len(values) > 10):
            print(column + ': ' + str(len(values)) + ' values')
        else:
            print(column + ': ' + str(values))

def CompressDataFrame(data):
    columnsToDrop = []
    for column in data.columns:
        if data[column].isnull().values.all():
            columnsToDrop.append(column)

    print('Dropping all the following columns since all NaN values')
    print(columnsToDrop)
    data.drop(columnsToDrop, axis = 1, inplace = True)

    for column in data.columns:
        if data[column].dtype == object:
            print(column + ': Upper Casing')
            data[column] = data[column].str.upper()
            continue

        HasNaN = data[column].isnull().values.any()
        minValue = data[column].min()
        maxValue = data[column].max()
        count = data[column].nunique()
        if ((count == 2) and (minValue == 0) and (maxValue == 1)):
            print(column + ': Converting to bool')
            data[column] = data[column].astype(bool)
            continue

        if HasNaN:
            if ((count == 1) and (minValue == maxValue) and (minValue == 1)):
                print(column + ': Converting to bool')
                data[column].fillna(0, inplace = True)
                data[column] = data[column].astype(bool)
                continue

            data[column].fillna(-1, inplace = True)
            minValue = data[column].min()

        info = np.iinfo
        if minValue >= 0:
            print(column + ': Converting to uint')
            types = (np.uint8, np.uint16, np.uint32, np.uint64)
        else:
            print(column + ': Converting to int')
            types = (np.int8, np.int16, np.int32, np.int64)

        for t in types:
            if info(t).min <= minValue and maxValue <= info(t).max:
                data[column] = data[column].astype(t)
                break

                
