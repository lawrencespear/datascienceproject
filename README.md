CS533 Introduction to Data Science 
==================================

* Kayla Krakoff - KaylaKrakoff@u.boisestate.edu
* Lawrence Spear - LawrenceSpear@u.boisestate.edu
* Tahsin Imtiaz - TahsinImtiaz@u.boisestate.edu

IPI3: Social Determinants of Healthcare Crises

Client's Description
====================

***Key question:*** How can Census geographical data and data on social determinants of health be used to identify and predict areas with healthcare crises?

***Practical benefit:*** This project will allow researchers and policy leaders to identify areas that are most in need of policy or programmatic interventions, allowing for a more proactive and preventative response to health crises.

***Data:*** US Census Bureau, Bureau of Vital Statistics, Robert Wood Johnson Foundation

Setup
=====

1. Import environment.yml to setup Anaconda environment
2. Following instructions in 0-Data Resources.ipynb on where to download the data from
3. Run the following Jupyter notebooks in order:
    1. 0-Data Resources.ipynb
    2. 1-Cleaning CDC data.ipynb
    3. 1-Cleaning County Mortality Data.ipynb

Usage
=====

Following the setup, you can run any of the other Jupyter notebooks to see the analysis we did

Results
=======

After searching for the medical and social determinates data, we were only able to find a large amount of publicly available data at the national level from CDC. We did locate a small dataset via the Census and the Idaho vital statistics page, but it only had four years of data which translates to four data points per county for a particular death cause. However, we were able to see the next health crises for Idaho with these four data points. Alzheimer's is currently #6 on the CDC top 10 leading causes of death for the Nation. But if the treads continue, and with the US aging population, it is likely that Alzheimer's will be moving into the top 5.

Unfortunately, we didn't have much data to do any meaningful analysis. So, with this lack of data, we decided to switch gears and help the next group to work on this problem as much as we could. We have located the CDC county level data request forms, but you'd have to request for the data a few weeks to months before the next team begins. Details on this is in the file 0-Data Resources.ipynb. Once that data is gathered, just update the MultipleCauseLayout.csv with the county data format and run 1-Cleaning CDC data.ipynb on the data.

MultipleCauseLayout.csv is a simple CSV that has two columns in it and describes the fixed width format the CDC uses. The first column is the length of the column and the second column is the name to use for that column. **Note:** column names must be unique

As for the social determinants, the Censes API only goes back to the last full census, which in our cause would have been seven years and that data isn't a complete survey either. Currently census has data only for a few of the Idaho's counties. To get data for all the counties, you have to wait until the Censes was done. We looked though the various government websites and couldn't locate a data source like we did from the CDC. 
 
As for the Robert Wood Johnson Foundation, we were never able to find a way to aquire their data.

If you have any questions about this deliverable, feel free to reach out to Lawrence Spear at the email address above and he'll happily answer your questions.

Reflection
==========

With the data being as sensitive as it is, it's very difficult to locate county level data. Even if the CDC county level data is collected, the future work would be hindered without a similar sized dataset for social determinants, which we weren't able to locate. However, this project could be updated to work at the national level data to be able to do tends with the aging population, comorbidity and other data located in the CDC's publicly available data. Or to look at larger trends at the national level utilizing the main Census surveys.
